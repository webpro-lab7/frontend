import http from './http'

type ReturnData = {
  celsius: number
  fahrenheit: number
}

async function convertTemp(c: number): Promise<number> {
  const res = await http.post(`/temperature/convert`, {
    celsius: c
  })
  const result = res.data as ReturnData
  return result.fahrenheit
}

export default { convertTemp }
