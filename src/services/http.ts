import axios from 'axios'

const instance = axios.create({
  baseURL: 'http://localhost:3000'
})

function delay(sec: number): Promise<void> {
  return new Promise((resolve) => {
    setTimeout(() => resolve(), sec * 1000)
  })
}

instance.interceptors.response.use(
  async function (res) {
    await delay(2)
    return res
  },
  function (err) {
    return Promise.reject(err)
  }
)

export default instance
