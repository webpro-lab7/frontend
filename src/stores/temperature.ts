import tempService from '@/services/temperature'
import { defineStore } from 'pinia'
import { computed, ref } from 'vue'
import { useLoadingStore } from './loading'

export const useTemperatureStore = defineStore('temperature', () => {
  const temp = ref(0)
  const fahrenheit = ref(0)
  const loading = useLoadingStore()

  const statusText = computed(() => {
    if (temp.value === 0 || temp.value === null) {
      return 'Empty number'
    }
    return ''
  })

  async function callConvert() {
    loading.doLoad()
    try {
      fahrenheit.value = await tempService.convertTemp(temp.value)
    } catch (e) {
      console.log('Error found: ', e)
    }
    loading.finishLoad()
  }
  return { temp, fahrenheit, statusText, callConvert }
})
